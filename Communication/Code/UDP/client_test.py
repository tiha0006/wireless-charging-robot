import threading
from udp_agent import *
import cl
from station import *
from station_client import *



agentc = UDP_Agent(8000+1,10)
agentc.start()

cl = Station_Client(agentc)
cl.start()


print("port: {0}".format(agentc.get_port()))

try:
    while(True):
        m = input()
        if(m and m[0] == '/'):
            if(m[1] == '+'):
                cl.charge_mode(True,0)
                continue
            if(m[1] == '-'):
                cl.charge_mode(False,0)
                continue
        agentc.send_queue.add(Message("127.0.0.1",agentc.get_port(),m.encode()))
except KeyboardInterrupt:
    pass


cl.stop()
agentc.stop()    

cl.join()
agentc.join()
#dumpthread.join()


