
# charging station protocol functions
def csp_parse_string(command_raw):
    command_raw = command_raw.decode("utf-8")
    if(not (command_raw.startswith("[") and command_raw.endswith("]"))):
        return None

    temp = [s.strip() for s in command_raw[1:-1].split(":")]
    if(len(temp)!=2):
        return None

    return [temp[0],temp[1].split(",")]

def csp_compile_command_string(name,args):
    return ("[{0}:{1}]".format(name,','.join(args))).encode()

