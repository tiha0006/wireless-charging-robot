
from udp_agent import *
import threading
import time 
from cs_protocol import *

import random
#import cl
from station_client import *

# a representation of a client to be used by a station
class Client():
        def __init__(self,interval=1,threshold=10):
            self.last_check = time.time()
            self.interval = interval
            self.threshold = threshold

        #sends a timeout check if a specified interval has elapsed
        def timeout_check(self,addport,udp_agent):
            if(self.bay != None):
                if(self.timeout_count >= self.threshold):
                    return True
                t = time.time()
                if t - self.last_check > self.interval:
                    self.timeout_count += 1
                    self.last_check += self.interval
                    #send bay response using check
                    cs = csp_compile_command_string("timeout_check",(str(udp_agent.get_port()),str(self.bay)))
                    udp_agent.send_queue.add(Message(addport[0],addport[1],cs))
                    return False
            return None
        
        def timeout_reset(self):
            self.timeout_count = 0

        bay = None
        timeout_count = 0
        
# handles charging station logic
# runs as a seperate thread
class Station(threading.Thread):
    
    def __init__(self, udp_agent, bays):
        threading.Thread.__init__(self)
        self.udp_agent = udp_agent
        self.clients = {}
        self.bays = [False]*bays
        self.progress = True
    
    def run(self):
        print("Station Thread - START")
        while(self.progress):
            # read newest message and process it
            message = self.udp_agent.read_queue.pop()
            if( message ):
                self.process_command(message[0][0],message[1])

            # check for clients that have timed out
            to_remove = [k for k,v in self.clients.items() if v.timeout_check(k,self.udp_agent)]
            for k in to_remove:
                if(self.clients[k].bay == None):
                    print("'None' bay on timeout?")
                    continue
                print("freeing bay {0}".format(self.clients[k].bay))
                self.bays[self.clients[k].bay] = False
                del self.clients[k]
                
        print("Station Thread - END")

    #stop thread
    def stop(self):
        self.progress = False

    # command:
    # station <-> client
    # > bayreq:listen_port,bat_percentage
    # < bayoffer:listen_port,baynum
    # < usagequery:listen_port,baynum
    # > ack:listen_port,yesno
    # format: [name:arg1,arg2,arg3]
    def process_command(self, source_address, data):
        #command to function call mapping
        action = { "bay_request":self.bay_request, "query_response":self.query_response }
        command_list = csp_parse_string(data)

        if(command_list == None):
            print("bad format", command_list)
            return False # bad format

        try:
            #execute action for command
            action[command_list[0]](source_address, command_list[1])
        except KeyError:
            print("unknown command: {0}".format(command_list[0]))
            return False
        except Exception as e:
            print(e,type(e))
            raise

        return True

    # get existing client
    def _get_client(self, source_address, listen_port):
        try:
            return self.clients[(source_address,listen_port)]
        except KeyError:
            return None

    # make new client
    def _new_client(self, source_address,listen_port):
        c = Client()
        c.port = listen_port
        self.clients[(source_address,listen_port)] = c
        return c

    #delete client
    def _del_client(self, source_address,listen_port):
        try:
            self.bays[self.clients[(source_address,listen_port)].bay] = False
            del self.clients[(source_address,listen_port)]
        except KeyError:
            print("delete fail - no such client")

    # bay request command
    def bay_request(self, source_address, data):
        #print("bay request", data)
        client = self._get_client(source_address, data[0])
        if(client == None):
            client = self._new_client(source_address, data[0])
        
        if(client.bay != None):
            if(client.bay >= len(self.bays) or client.bay < 0 or self.bays[client.bay] == False):
                print(client.bay, self.bays[client.bay],self.bays,"client with invalid bay!")
                #remove client!
                return
        else:
            if(self.assign_free_bay(client) == None):
                print("no free bay slot!")
                return
        cs = csp_compile_command_string("assigned_bay",(str(self.udp_agent.get_port()),str(client.bay)))
        self.udp_agent.send_queue.add(Message(source_address,data[0],cs))
            
    # allocate free bay
    def assign_free_bay(self, client):
        indices = []

        for i in range(len(self.bays)):
            if not self.bays[i]: 
                indices.append(i)
        
        if len(indices) == 0:
            return None
        
        selectedindex = random.randint(0,len(indices)-1)

        client.bay = indices[selectedindex]      # assign bay
        self.bays[indices[selectedindex]] = True # reserve bay
        return indices[selectedindex]

    # query response command
    def query_response(self, source_address, data):
        if(len(data) != 2):
            print("invalid arg count")
        client = self._get_client(source_address,data[0])
        if(not client):
            print("no such client")
            return 

        if(data[1] == "used"):
            client.timeout_reset() # client.timeout_reset()
            return
        if(data[1] == "free"):
            self._del_client(source_address,data[0])
            return
        

# start station 
if(__name__ == "__main__"):
    agent = UDP_Agent(8000,10)
    agent.start()

    st = Station(agent,2)
    st.start()

    print("port: {0}".format(agent.get_port()))

    try:
        while(True):
            m = input()
            if(m and m[0] == '/'):
                if(m[1] == 'c'):
                    for k,v in st.clients.items():
                        print(k,v)
                    print(st.bays)
                    continue
            print("not sent {0}".format(m)) 
    except KeyboardInterrupt:
        pass
    except:
        pass    

    agent.stop()   
    st.stop()

    agent.join()
    st.join()
