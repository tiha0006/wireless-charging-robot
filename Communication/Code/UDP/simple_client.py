from station_client import *
import sys

#a simplified interface for setting up a station client reduced to the 4 essential functions with the minimum of arguments
class Simple_Client():
    def __init__(self, server_address, port_start = 8001, port_range = 10):
        self.agent = UDP_Agent(port_start,port_range)
        self.agent.start()

        self.cl = Station_Client(self.agent, server_address)
        self.cl.start()
        
        # exposing the relevant station client interface functions
        self.request_charging_bay = self.cl.charge_mode
        self.get_assigned_bay = self.cl.get_designated_bay

    #stops relevant threads
    def stop(self):
        self.cl.stop()
        self.agent.stop()    

        self.cl.join()
        self.agent.join()

# threaded, buffered input to be used as non blocking input collection
class Async_Input(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.lock = threading.Lock()
        self.buffer = []
        self.proceed = True
    def run(self):
        while(self.proceed):
            i = input()
            self.lock.acquire()
            self.buffer.append(i)
            self.lock.release()
            time.sleep(0)
    def get_input(self):
        self.lock.acquire()
        if(len(self.buffer)):
            line = self.buffer[0]
            self.buffer = self.buffer[1:]
            self.lock.release()
            return line
        self.lock.release()
        return None
    def stop(self):
        self.proceed = False


if(__name__ == "__main__"):

    ainput = Async_Input()
    ainput.start()

    try:
        x = 0
        while(True):
            time.sleep(3)
            print(x)#, end="")
            sys.stdout.flush()
            x+=1

            while(True):
                inp = ainput.get_input()
                if(inp):
                    print(inp)
                else:
                    break
                
    except KeyboardInterrupt:
        pass
    ainput.stop()

    client = Simple_Client("127.0.0.1")

    client.request_charging_bay(True,0.5)
    
    while(client.get_assigned_bay() == None):
        pass

    print("Assigned bay:", client.get_assigned_bay())

    time.sleep(5)
    client.request_charging_bay(False,0.5)

    while(client.get_assigned_bay() != None):
        pass

    print("Assigned bay:", client.get_assigned_bay())

    time.sleep(1)
    client.stop()
    print("done!")

