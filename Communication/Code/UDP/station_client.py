import threading
from udp_agent import *
from cs_protocol import *
import time
import random

# station client class
class Station_Client(threading.Thread):
    def __init__(self,udp_agent,server_address = "127.0.0.1", interval = 1,offer_collect_wait = 5):
        threading.Thread.__init__(self)
        self.udp_agent = udp_agent
        self.server_address = server_address
        self.interval = interval
        self.offer_collect_wait = offer_collect_wait
        self.charging_station_offers = {}
        self.charge_mode_val = False
        self.battery = 0
        self.proceed = True
        self.collect_offers_start_time = 0
        self.designated_bay = None

    # stop thread
    def stop(self):
        self.proceed = False

    def run(self):
        print("Station Client Thread - Start")
        self.last_t = time.time()
        while(self.proceed):
            message = self.udp_agent.read_queue.pop()
            if( message ):
                self.process_command(message[0][0],message[1])

            if self.charge_mode_val == True:
                t = time.time()
                #if the min wait period is over and there has been at least one offer, accept one
                if self.collect_offers_start_time + self.offer_collect_wait <= t and len(self.charging_station_offers) > 0:
                    #accept offer
                    if(self.designated_bay == None):
                        print("BAY DESIGNATED")
                        keys = list(self.charging_station_offers.keys())
                        selected = keys[random.randint(0,(len(keys))-1)]
                        self.charging_station_offers = {selected:self.charging_station_offers[selected]}
                        self.designated_bay = self.charging_station_offers[selected]
                # else keep trying
                else:
                    if self.last_t + self.interval <= t:
                        self.last_t += self.interval
                        cs = csp_compile_command_string("bay_request",(str(self.udp_agent.get_port()),str(self.battery)))
                        #self.udp_agent.send_queue.add(Message("255.255.255.255",8000,cs)) # default port
                        self.udp_agent.send_queue.add(Message(self.server_address,8000,cs)) # default port
            time.sleep(0.2)

        print("Station Client Thread - End")

    # take action based on the command received
    def process_command(self, source_address, data):
        action = { "assigned_bay":self.assigned_bay, "timeout_check":self.timeout_check }
        command_list = csp_parse_string(data)

        if(command_list == None):
            print("bad format", command_list)
            return False # bad format

        try:
            action[command_list[0]](source_address, command_list[1])
        except KeyError:
            print("unknown command: {0}".format(command_list[0]))
            return False
        except Exception as e:
            print(e,type(e))
            raise

        return True
    
    # set charge mode to true/false, this can be used to trigger bay requests
    def charge_mode(self, val, battery):
        if(type(val) != bool):
            print("invalid charge mode",val,type(val))
            return 
        if(self.charge_mode_val == False and val == True):
            print("starting offer collection period")
            self.last_t = time.time()
            self.collect_offers_start_time = time.time()
        elif(self.charge_mode_val == True and val == False):
            print("ending bay reservation")
            self.designated_bay = None
            self.charging_station_offers.clear()
        self.charge_mode_val = val
        self.battery = battery

    # assigned bay command received
    def assigned_bay(self, source_address, data):
        if self.designated_bay == None:
            try:
                x = int(data[1])
                self.charging_station_offers[(source_address,data[0])] = x
            except ValueError:
                print("value error converting {0} to int".format(data[1]))
        else:
            print("not accepting new bay offers; {0} already assigned".format(self.designated_bay))
    
    # timeout received command received
    def timeout_check(self, source_address, data):
        if (source_address,data[0]) in self.charging_station_offers:
            cs = csp_compile_command_string("query_response",(str(self.udp_agent.get_port()),str("used")))
        else:
            cs = csp_compile_command_string("query_response",(str(self.udp_agent.get_port()),str("free")))
        self.udp_agent.send_queue.add(Message(source_address,data[0],cs)) # default port

    # return the bay value retrieved by the station client
    def get_designated_bay(self):
        return self.designated_bay