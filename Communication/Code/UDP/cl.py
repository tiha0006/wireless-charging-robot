# Description:
# a module for parsing and processing command lines
import sys

def argtotype(s):
    try:
        return int(s)
    except ValueError:
        pass

    try:
        return float(s)
    except ValueError:
        pass
    return s

def isnum(s):
    try:
        int(s)
        return True
    except ValueError:
        pass

    try:
        float(s)
        return True
    except ValueError:
        pass

    return False

def collectargs(a):
    if len(a) > 1 and a[1][0] == '-' and not isnum(a[1]): return a[0:1]
    elif len(a) == 1: return a
    return a[0:1] + collectargs(a[1:])

def parseargv(argv = sys.argv[1:]):
    #argv = sys.argv[1:]
    cl = {}
    a = []
    for i in range(len(argv)):
        if argv[i][0] == '-' and not isnum(argv[i]):
            floop = collectargs(argv[(i):])
            cl.update({floop[0]: floop[1:]})

    return cl

cl = parseargv()
