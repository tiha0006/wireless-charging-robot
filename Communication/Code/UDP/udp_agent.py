import threading
import socket
import time

# message data container
class Message():
    def __init__(self, address, port, data):
        self.address = address
        self.port = port
        self.data = data

# thread safe queue
class Thread_Queue:
    def __init__(self, lock = None):
        self.lock = (threading.Lock() if lock == None else lock)
        self.queue = []
    
    def add(self, element):
        self.lock.acquire()
        self.queue.append(element)
        self.lock.release()

    def pop(self):
        self.lock.acquire()
        try:
            element = self.queue.pop(0)
        except IndexError:
            element = None
        self.lock.release()
        return element

# Class responsible for sending and receiving UDP messages
class UDP_Agent(threading.Thread):
    def __init__(self, listen_port, bind_attempts):
        threading.Thread.__init__(self)
        self.read_queue = Thread_Queue()
        self.send_queue = Thread_Queue()
        self.proceed = True
        
        self.bind_udp(listen_port, bind_attempts)
        self.socket.setblocking(0)
        
    
    def run(self):
        self.threadID = threading.get_ident()
        print("UDP Agent - start({0})".format(self.threadID))

        while(self.proceed):
            # send
            while(True):
                message = self.send_queue.pop()
                if(message == None):
                    break
                print(">>:", (message.address),(message.port),(message.data.decode()))
                self.socket.sendto(message.data,(message.address,int(message.port)))
            # receive
            while(True):
                try:
                    data, address = self.socket.recvfrom(1024)
                except socket.error:
                    break
                print("<<:", *address, data.decode())
                self.read_queue.add((address, data))
            time.sleep(0.2)

        print("UDP Agent - end")
    # stop thread
    def stop(self):
        self.proceed = False

    # configure udp listen port
    def bind_udp(self,portnum,attempts):
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        #self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        while(True):
            try: 
                self.socket.bind(("",portnum))
            except socket.error as e:
                if(attempts == 0):
                    raise
                print("Port {0} in use ({1} attempts remaining)".format(portnum,attempts))
                portnum += 1
                attempts -= 1                
                continue
            break
    
    # return port value
    def get_port(self):
        return self.socket.getsockname()[1]



