#!/usr/bin/env python3
import RPi.GPIO as GPIO
from time import sleep
import time
import numpy as np
import cv2
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish # For sending station color to car
import robot_with_navi 


GPIO.setmode(GPIO.BOARD)

FMotor1A = 37
FMotor1B = 36
FMotor1Enable = 31

BMotor1A = 16
BMotor1B = 29
BMotor1Enable = 18

FMotor2A = 33
FMotor2B = 35
FMotor2Enable = 40

BMotor2A = 11
BMotor2B = 7
BMotor2Enable = 13

GPIO.setup(FMotor1A,GPIO.OUT)
GPIO.setup(FMotor1B,GPIO.OUT)
GPIO.setup(FMotor1Enable,GPIO.OUT)

GPIO.setup(BMotor1A,GPIO.OUT)
GPIO.setup(BMotor1B,GPIO.OUT)
GPIO.setup(BMotor1Enable,GPIO.OUT)

GPIO.setup(FMotor2A,GPIO.OUT)
GPIO.setup(FMotor2B,GPIO.OUT)
GPIO.setup(FMotor2Enable,GPIO.OUT)

GPIO.setup(BMotor2A,GPIO.OUT)
GPIO.setup(BMotor2B,GPIO.OUT)
GPIO.setup(BMotor2Enable,GPIO.OUT)

def Fright():
    GPIO.output(FMotor2A,GPIO.LOW)
    GPIO.output(FMotor2B,GPIO.HIGH)
    GPIO.output(FMotor2Enable,GPIO.HIGH)

def Frightb():
    GPIO.output(FMotor2A,GPIO.HIGH)
    GPIO.output(FMotor2B,GPIO.LOW)
    GPIO.output(FMotor2Enable,GPIO.HIGH)

def Fleft():
    GPIO.output(FMotor1A,GPIO.LOW)
    GPIO.output(FMotor1B,GPIO.HIGH)
    GPIO.output(FMotor1Enable,GPIO.HIGH)

def Fleftb():
    GPIO.output(FMotor1A,GPIO.HIGH)
    GPIO.output(FMotor1B,GPIO.LOW)
    GPIO.output(FMotor1Enable,GPIO.HIGH)

def Fmotors():
    Fright()
    Fleft()

def Bright():
    GPIO.output(BMotor1A,GPIO.LOW)
    GPIO.output(BMotor1B,GPIO.HIGH)
    GPIO.output(BMotor1Enable,GPIO.HIGH)

def Brightb():
    GPIO.output(BMotor1A,GPIO.HIGH)
    GPIO.output(BMotor1B,GPIO.LOW)
    GPIO.output(BMotor1Enable,GPIO.HIGH)

def Bleft():
    GPIO.output(BMotor2A,GPIO.LOW)
    GPIO.output(BMotor2B,GPIO.HIGH)
    GPIO.output(BMotor2Enable,GPIO.HIGH)

def Bleftb():
    GPIO.output(BMotor2A,GPIO.HIGH)
    GPIO.output(BMotor2B,GPIO.LOW)
    GPIO.output(BMotor2Enable,GPIO.HIGH)

def Bmotors():
    Bright()
    Bleft()

def Stop():
    GPIO.output(FMotor1Enable, GPIO.LOW)
    GPIO.output(FMotor2Enable, GPIO.LOW)
    GPIO.output(BMotor1Enable, GPIO.LOW)
    GPIO.output(BMotor2Enable, GPIO.LOW)

def Drive(contours, cx, cy):
    cv2.line(img,(cx,0),(cx,720),(255,0,0),1)
    cv2.line(img,(0,cy),(1280,cy),(255,0,0),1)
    cv2.drawContours(img, contours, -1, (0,255,0), 1)
    if cx >= 120:
 #        print("right")
        Fleft()
        Bleft()
        Frightb()
        Brightb()
    if cx < 120 and cx > 50:
 #        print("straight")
        Fright()
        Fleft()
        Bright()
        Bleft()
    if cx <= 50:
 #        print("left")
        Fright()
        Bright()
        Fleftb()
        Bleftb()

def Controls():
    if len(contoursC) > 0:
 #        print("Tracking color")
        c = max(contoursC, key=cv2.contourArea)
 #        print(cv2.contourArea(c))
        if cv2.contourArea(c) < 300:
            if len(contoursB) > 0:
                c = max(contoursB, key=cv2.contourArea)

        M = cv2.moments(c)
        if M["m00"] != 0:
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
        else:
            cx,cy = 0, 0

        Drive(contoursC, cx, cy)

    elif len(contoursB) > 0:
 #        print("Tracking black")
        c = max(contoursB, key=cv2.contourArea)
        M = cv2.moments(c)
        if M["m00"] != 0:
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
        else:
            cx,cy = 0, 0

        Drive(contoursB, cx, cy)

    else:
 #        print("stop")
        Stop()

mqttclient = robot_with_navi.mqttSetup()

user = 2

def mqtt_callback(message):
    global user
    actions = {"track blue":1, "track black":2, "track pink":3}
    if(message in actions):
        user = actions[message]
    print(">>",message)
    
robot_with_navi.navi_callback = mqtt_callback

cap = cv2.VideoCapture(-1)
cap.set(3, 160)
cap.set(4, 120)

#user = int(input("1 for blue\n2 for black\n3 for pink\nMake your choice: "))

try:
    while(True):

        mqttclient.loop(0.01)
        # Capture the frames
        ret, img = cap.read()

        crop_img = img[60:120, 0:160]

        # Converting frame(img i.e BGR) to HSV (hue-saturation-value)
        hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

        # Definig the range of blue colour
        blue_lower = np.array([83,83,130],np.uint8)
        blue_upper = np.array([150,255,255],np.uint8)

        # Defining the range of black colour
        black_lower = np.array([0,0,0],np.uint8)
        black_upper = np.array([50,50,100],np.uint8)

        # Defining the range of pink colour
        pink_lower = np.array([150,48,86],np.uint8)
        pink_upper = np.array([255,100,255],np.uint8)

        # Finding the range of blue, black and pink colour in the image
        if user == 1:
            blue = cv2.inRange(hsv, blue_lower, blue_upper)
        elif user == 3:
            pink = cv2.inRange(hsv, pink_lower,pink_upper)

        black = cv2.inRange(hsv, black_lower,black_upper)

        # Morphological transformation, dilation
        kernal = np.ones((5 ,5), "uint8")
        if user == 1:
            blue = cv2.dilate(blue, kernal)
        elif user == 3:
            pink = cv2.dilate(pink, kernal)

        black = cv2.dilate(black, kernal)

        contoursC = []
        contoursB = []

        if user == 1:
            # Tracking the blue colour
            (contoursC,hierarchyC) = cv2.findContours(blue,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

            for pic, contour in enumerate(contoursC):
                area = cv2.contourArea(contour)
                if(area>300):
                    x,y,w,h = cv2.boundingRect(contour)
                    img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)

        elif user == 3:
            # Tracking the pink colour
            (contoursC,hierarchyC) = cv2.findContours(pink,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

            for pic, contour in enumerate(contoursC):
                area = cv2.contourArea(contour)
                if(area>300):
                    x,y,w,h = cv2.boundingRect(contour)
                    img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)

        # Tracking the black colour
        (contoursB,hierarchyB) = cv2.findContours(black,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

        for pic, contour in enumerate(contoursB):
            area = cv2.contourArea(contour)
            if(area>300):
                x,y,w,h = cv2.boundingRect(contour)
                img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)

#        print("Color: ", len(contoursC))

        if user == 1:
            Controls()
        elif user == 2:
            Controls()
        elif user == 3:
            Controls()
        else:
            Stop()

 #       # Display the resulting frame
 #       cv2.imshow('frame',img)
 #       if cv2.waitKey(1) & 0xFF == ord('q'):
 #           break

except KeyboardInterrupt:
    Stop()
    GPIO.cleanup()
