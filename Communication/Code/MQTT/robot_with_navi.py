 
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish # For sending station color to car


navi_callback = None

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # reconnect then subscriptions will be renewed.
    client.subscribe("request charging")
    
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload.decode()))

    colors = ["track blue", "track pink", "track black"]
    
    if msg.payload.decode() in colors:
       if navi_callback != None:
            navi_callback(msg.payload.decode())

def mqttSetup():
    # Create an MQTT client and attach our routines to it.
    
    client = mqtt.Client("robot")
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("localhost", 1883, 60)
    return client



