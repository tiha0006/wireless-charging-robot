 
#import signal, sys
import paho.mqtt.client as mqtt
import paho.mqtt.publish as publish # For sending station color to car
#import RPi.GPIO as GPIO	# To use GPIO pins

#led_pin_blue = 21
#GPIO.setmode(GPIO.BCM)#
#
#def terminateProcess(si#gnalNumber, frame):
#    print ('(SIGTERM) t#erminating the process')
#    GPIO.cleanup()
#    sys.exit()

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))

    # reconnect then subscriptions will be renewed.
    client.subscribe("request charging")
    
# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload.decode()))

    if msg.payload.decode() == "turn on blue":
        print("Received message:", msg.payload.decode())
       # GPIO.output(led_pin_blue,GPIO.HIGH)
        publish.single("ack response", "turn on green", hostname="localhost")
    elif msg.payload.decode() == "turn off blue":
        print("Received message:", msg.payload.decode())
        #GPIO.output(led_pin_blue,GPIO.LOW)
        publish.single("ack response", "turn off green", hostname="localhost")

def mqttSetup():
    # Create an MQTT client and attach our routines to it.
    
    client = mqtt.Client("station")
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("localhost", 1883, 60)
    client.loop_forever()

def main():
   # GPIO.setup(led_pin_blue, GPIO.OUT)
   # GPIO.output(led_pin_blue,GPIO.LOW)
   # signal.signal(signal.SIGTERM, terminateProcess)
    mqttSetup()


if __name__ == "__main__":
    main()
