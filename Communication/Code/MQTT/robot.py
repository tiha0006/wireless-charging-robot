# Importing modules
#import spidev # To communicate with SPI devices
#from numpy import interp	# To scale values
#from time import sleep,time	# To add delay
#import RPi.GPIO as GPIO	# To use GPIO pins
import paho.mqtt.publish as publish # For publishing values
import paho.mqtt.client as mqtt # To receive message from station

## Start SPI connection
#spi = spidev.SpiDev() # Created an object
#spi.open(0,0)	
## Initializing LED pin as OUTPUT pin
#led_pin_red = 20
#led_pin_green = 16
##GPIO.setwarnings(False)
#GPIO.setmode(GPIO.BCM)
#GPIO.setup(led_pin_red, GPIO.OUT)
#GPIO.setup(led_pin_green, GPIO.OUT)
#GPIO.output(led_pin_green,GPIO.LOW)
#
## Creating a PWM channel at 100Hz frequency
#pwm = GPIO.PWM(led_pin_red, 100)
#pwm.start(0) 
#
#
## Read MCP3008 data
#def analogInput(channel):
#    spi.max_speed_hz = 1350000
#    adc = spi.xfer2([1,(8+channel)<<4,0])
#    data = ((adc[1]&3) << 8) + adc[2]
#    return data

def main(client):
    state, lastState = 0, 0
    while True:
        #output = analogInput(0) # Reading from CH0
        #output = interp(output, [0, 1023], [0, 100])
        #print(output)
        #pwm.ChangeDutyCycle(output)
        #client.loop(2)
        output = int(input("input for testing: "))
        

        # This is the Publisher

        if output < 20: #if value is below 20 send message
            state = 1
        else:
            state = 0

        if state != lastState:
            lastState = state
            if state:
                publish.single("request charging", "turn on blue", hostname="localhost")
            else:
                publish.single("request charging","turn off blue", hostname="localhost")
        

#Subscribing for stations message
def on_connect(client, userdata, flags, rc):
# reconnect then subscriptions will be renewed.
    client.subscribe("ack response")

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload.decode()))

    if msg.payload.decode() == "turn on green":
        print("Received message:", msg.payload.decode())
       # GPIO.output(led_pin_green,GPIO.HIGH)
    elif msg.payload.decode() == "turn off green":
        print("Received message:", msg.payload.decode())
      #  GPIO.output(led_pin_green,GPIO.LOW)

def mqttSetup():
    # Create an MQTT client and attach our routines to it.
    client = mqtt.Client("robot")
    client.on_connect = on_connect
    client.on_message = on_message
    client.connect("localhost", 1883, 60)
    return client
    
   
    
if __name__ == "__main__":
    client = mqttSetup()
    main(client)
