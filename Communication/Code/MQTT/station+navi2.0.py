# Importing modules
import spidev # To communicate with SPI devices
from numpy import interp	# To scale values
from time import sleep,time	# To add delay
import paho.mqtt.publish as publish # For publishing values
import paho.mqtt.client as mqtt # To receive message from station
import random

# Start SPI connection
spi = spidev.SpiDev() # Created an object
spi.open(0,0)	

# Read MCP3008 data
def analogInput(channel):
    spi.max_speed_hz = 1350000
    adc = spi.xfer2([1,(8+channel)<<4,0])
    data = ((adc[1]&3) << 8) + adc[2]
    return data

def main(client):

    while True:
        output = analogInput(0) # Reading from CH0
        output = interp(output, [0, 1023], [0, 100])
        print(output)
  
        state, lastState = 0, 0

        # This is the Publisher
                

        if output < 20: #if value is below 20 send message
            colors = ["track blue", "track pink"]
            track = random.choice(colors)

            state = 1
        else:
            state = 0

        if state != lastState:
            lastState = state
            
            if state:
             publish.single("request charging", track, hostname="localhost")
        else:
            publish.single("request charging","track black", hostname="localhost")
        

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    print(msg.topic+" "+str(msg.payload.decode()))

    if msg.payload.decode() == "message received":
        print("Received message:", msg.payload.decode())
     
def mqttSetup():
    # Create an MQTT client and attach our routines to it.
    client = mqtt.Client("station")
   # client.on_connect = on_connect
    client.on_message = on_message
    client.connect("localhost", 1883, 60)
    return client
    
   
    
if __name__ == "__main__":
    client = mqttSetup()
    main(client)
