---
title: 'Wireless_Charging_Robot'
subtitle: 'Project Plan'
authors: ['Laurynas Medvedevas laur176n@edu.ucl.dk', 'Anthony Peak anth0662@edu.ucl.dk', 'Tihamer Biliboc tiha0006@edu.ucl.dk', 'Sebastian Mason seba7286@edu.ucl.dk', 'Karthiebhan Mahendran kart0050@edu.ucl.dk', 'Henrijs Valdovskis laur177n@edu.ucl.dk']
date: \today
left-header: \today
right-header: Project Plan
skip-toc: false
---



# Background

This is the 3rd semester project where we will develop a wireless charging solution for a robot car.

# Goals

The overall system that is going to be build looks as follows:

![Block_Diagram](https://gitlab.com/tiha0006/wireless-charging-robot/raw/master/Project_Management/Pictures/block_diagram.png)

The block diagram showcased above displays the main idea for how the charging circuit will function in the project. First, power from a power source, such as a solar panel, is used to charge a power bank on the charging station. Then, when a sensor picks up that something is on the charging station, in this case, the robot car, it starts charging the onboard battery of the robot car using wireless charging coils (one coil is present on the charging station and the other is on the car itself). 


Project deliveries are:
* The robot can communicate with different charging stations
* Renewable source of electric
* Power transformation and storage
* Wireless power transfer
* Charging and storing power on target
* Documentation of all parts of the solution
* Final evaluation




# Schedule

The project is divided into three phases from the start of September 2019 to December 2019.

See the lecture plan for details.


# Organization
## Group Members
- Laurynas Medvedevas
- Anthony Peak
- Tihamer Biliboc
- Sebastian Mason
- Karthiebhan Mahendran
- Henrijs Valdovskis

## Advisors / Overseers
- Ilias Esmati
- Ruslan Trifonov

## External resources
### Knowledge
- Classmates
- Teachers

# Budget and resources

Components will be provided by UCL. If special components are required a proposal will be made to Copenhagen Technologies to share the expenses.


# Risk assessment

Risk assessment is a list of possible risks to the project, and suggestions on how to solve them.

## Project risks

- Teamwork issues
- Lack of documentation
- Poor project management
- Lack of knowledge
- Lack of motivation
- Hardware failures
- Sick leave
- First Semesters being bad

## How to overcome these issues

### Teamwork
- Meetings
- Reorganizing and restructuring tasks
- Mentoring
- Defining clear expectations
- Talk to advisors
- Remove any non participating members

### Documentation

- Inform the parties from whom we require more information

### Project Management

- Make sure to keep group meetings a regular thing so everyone is on the same page
- Never leave issues unresolved! make priorities

### Knowledge

- Seek knowledge / help from class mates.
- Be honest with the goals, never hide your progress!
- Reduce scope of the tasks/project

### Hardware Failures
- Make backups
- Have spare parts ready
- Triple check everything
- Don't make high risk decisions

### Sick Leave
- Keep contact with absent members
- Redistribute workload to compensate for absent members
- Try to work from home if possible

### First Semesters being bad
- Mentoring
- Explanations

# Stakeholders
- Laurynas Medvedevas
- Anthony Peak
- Tihamer Biliboc
- Sebastian Mason
- Karthiebhan Mahendran
- Henrijs Valdovskis
- lias Esmati
- Ruslan Trifonov
- Julie Bloch Jensen

## Project coordinators 
- lias Esmati
- Ruslan Trifonov

# Communication

## Group members
All communication within the group will take place via Issues on Gitlab, Discord, Messenger or other messaging applications


## Project coordinators
Communication to and from the project coordinators will manifest mainly via weekly meetings, itslearning and e-mails

## Customers
Communication to and from the customers will manifest mainly via e-mail, phone or in person

# Perspectives

This project can be used as an example of the students abilities in their portfolio, when applying for jobs or internships.

It is also used as good practice for project management, since we will be closely working with the first semester.

# Evaluation

The project is a success if the group manages to complete the following goals:

### Group Evaluation

- The members each had an equal ammount of work
- Communication was succesful, everyone was on the same page

### Process Evaluation

- Wireless power transfer was achieved
- Charging stations can communicate with the car
- The car can find a charging station and top up its battery autonomously 

### End Evaluation

- Everything was well documented
- Every main part of the project was succesfully fulfilled

# References

None at this time
