#!/usr/bin/env python3
import RPi.GPIO as GPIO
from time import sleep
import time
import numpy as np
import cv2

GPIO.setmode(GPIO.BOARD)

Front_Enable_1_and_2 = 3
Front_In_1 = 5
Front_In_2 = 7

Front_Enable_3_and_4 = 11
Front_In_3 = 13
Front_In_4 = 15

Back_Enable_1_and_2 = 12
Back_In_1 = 16
Back_In_2 = 18

Back_Enable_3_and_4 = 22
Back_In_3 = 24
Back_In_4 = 26

GPIO.setup(Front_Enable_1_and_2,GPIO.OUT)
GPIO.setup(Front_In_1,GPIO.OUT)
GPIO.setup(Front_In_2,GPIO.OUT)

GPIO.setup(Front_Enable_3_and_4,GPIO.OUT)
GPIO.setup(Front_In_3,GPIO.OUT)
GPIO.setup(Front_In_4,GPIO.OUT)

GPIO.setup(Back_Enable_1_and_2,GPIO.OUT)
GPIO.setup(Back_In_1,GPIO.OUT)
GPIO.setup(Back_In_2,GPIO.OUT)

GPIO.setup(Back_Enable_3_and_4,GPIO.OUT)
GPIO.setup(Back_In_3,GPIO.OUT)
GPIO.setup(Back_In_4,GPIO.OUT)

def Motor_1_Forwards():
    GPIO.output(Front_Enable_1_and_2,GPIO.HIGH)
    GPIO.output(Front_In_1,GPIO.HIGH)
    GPIO.output(Front_In_2,GPIO.LOW)

def Motor_1_Backwards():
    GPIO.output(Front_Enable_1_and_2,GPIO.HIGH)
    GPIO.output(Front_In_1,GPIO.LOW)
    GPIO.output(Front_In_2,GPIO.HIGH)

def Motor_2_Forwards():
    GPIO.output(Front_Enable_3_and_4,GPIO.HIGH)
    GPIO.output(Front_In_3,GPIO.HIGH)
    GPIO.output(Front_In_4,GPIO.LOW)

def Motor_2_Backwards():
    GPIO.output(Front_Enable_3_and_4,GPIO.HIGH)
    GPIO.output(Front_In_3,GPIO.LOW)
    GPIO.output(Front_In_4,GPIO.HIGH)

def Motor_3_Forwards():
    GPIO.output(Back_Enable_1_and_2,GPIO.HIGH)
    GPIO.output(Back_In_1,GPIO.HIGH)
    GPIO.output(Back_In_2,GPIO.LOW)

def Motor_3_Backwards():
    GPIO.output(Back_Enable_1_and_2,GPIO.HIGH)
    GPIO.output(Back_In_1,GPIO.LOW)
    GPIO.output(Back_In_2,GPIO.HIGH)

def Motor_4_Forwards():
    GPIO.output(Back_Enable_3_and_4,GPIO.HIGH)
    GPIO.output(Back_In_3,GPIO.HIGH)
    GPIO.output(Back_In_4,GPIO.LOW)

def Motor_4_Backwards():
    GPIO.output(Back_Enable_3_and_4,GPIO.HIGH)
    GPIO.output(Back_In_3,GPIO.LOW)
    GPIO.output(Back_In_4,GPIO.HIGH)

def Right():
    Motor_1_Forwards()
    Motor_2_Backwards()
    Motor_3_Backwards()
    Motor_4_Forwards()

def Left():
    Motor_1_Backwards()
    Motor_2_Forwards()
    Motor_3_Forwards()
    Motor_4_Backwards()

def Straight():
    Motor_1_Forwards()
    Motor_2_Forwards()
    Motor_3_Forwards()
    Motor_4_Forwards()

def Stop():
    GPIO.output(Front_Enable_1_and_2,GPIO.LOW)
    GPIO.output(Front_Enable_3_and_4,GPIO.LOW)
    GPIO.output(Back_Enable_1_and_2,GPIO.LOW)
    GPIO.output(Back_Enable_3_and_4,GPIO.LOW)

def Drive(contours, cx, cy):
    cv2.line(img,(cx,0),(cx,720),(255,0,0),1)
    cv2.line(img,(0,cy),(1280,cy),(255,0,0),1)
    cv2.drawContours(img, contours, -1, (0,255,0), 1)
    if cx >= 120:
        Right()

    if cx < 120 and cx > 50:
        Straight()

    if cx <= 50:
        Left()

def Controls():
    if len(contoursC) > 0:
        c = max(contoursC, key=cv2.contourArea)
        if cv2.contourArea(c) < 300:
            if len(contoursB) > 0:
                c = max(contoursB, key=cv2.contourArea)

        M = cv2.moments(c)
        if M["m00"] != 0:
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
        else:
            cx,cy = 0, 0

        Drive(contoursC, cx, cy)

    elif len(contoursB) > 0:
        c = max(contoursB, key=cv2.contourArea)
        M = cv2.moments(c)
        if M["m00"] != 0:
            cx = int(M['m10']/M['m00'])
            cy = int(M['m01']/M['m00'])
        else:
            cx,cy = 0, 0

        Drive(contoursB, cx, cy)

    else:
        Stop()

cap = cv2.VideoCapture(-1)
cap.set(3, 160)
cap.set(4, 120)

user = int(input("1 for blue\n2 for black\n3 for pink\nMake your choice: "))

try:
    while(True):
        # Capture the frames
        ret, img = cap.read()

        crop_img = img[60:120, 0:160]

        # Converting frame(img i.e BGR) to HSV (hue-saturation-value)
        hsv = cv2.cvtColor(img,cv2.COLOR_BGR2HSV)

        # Definig the range of blue colour
        blue_lower = np.array([83,83,130],np.uint8)
        blue_upper = np.array([150,255,255],np.uint8)

        # Defining the range of black colour
        black_lower = np.array([0,0,0],np.uint8)
        black_upper = np.array([50,50,100],np.uint8)

        # Defining the range of pink colour
        pink_lower = np.array([150,48,86],np.uint8)
        pink_upper = np.array([255,100,255],np.uint8)

        # Finding the range of blue, black and pink colour in the image
        if user == 1:
            blue = cv2.inRange(hsv, blue_lower, blue_upper)
        elif user == 3:
            pink = cv2.inRange(hsv, pink_lower,pink_upper)

        black = cv2.inRange(hsv, black_lower,black_upper)

        # Morphological transformation, dilation
        kernal = np.ones((5 ,5), "uint8")
        if user == 1:
            blue = cv2.dilate(blue, kernal)
        elif user == 3:
            pink = cv2.dilate(pink, kernal)

        black = cv2.dilate(black, kernal)

        contoursC = []
        contoursB = []

        if user == 1:
            # Tracking the blue colour
            (contoursC,hierarchyC) = cv2.findContours(blue,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

            for pic, contour in enumerate(contoursC):
                area = cv2.contourArea(contour)
                if(area>300):
                    x,y,w,h = cv2.boundingRect(contour)
                    img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,255),2)

        elif user == 3:
            # Tracking the pink colour
            (contoursC,hierarchyC) = cv2.findContours(pink,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

            for pic, contour in enumerate(contoursC):
                area = cv2.contourArea(contour)
                if(area>300):
                    x,y,w,h = cv2.boundingRect(contour)
                    img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)

        # Tracking the black colour
        (contoursB,hierarchyB) = cv2.findContours(black,cv2.RETR_TREE,cv2.CHAIN_APPROX_SIMPLE)

        for pic, contour in enumerate(contoursB):
            area = cv2.contourArea(contour)
            if(area>300):
                x,y,w,h = cv2.boundingRect(contour)
                img = cv2.rectangle(img,(x,y),(x+w,y+h),(0,0,0),2)

        if user == 1:
            Controls()
        elif user == 2:
            Controls()
        elif user == 3:
            Controls()
        else:
            Stop()

        # Display the resulting frame
        cv2.imshow('frame',img)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break

except KeyboardInterrupt:
    Stop()
    GPIO.cleanup()